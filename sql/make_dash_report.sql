USE `dcr-status`;
truncate table dashboard_report;
insert into dashboard_report 
select a.*,
case when b.pointvalue is not null then b.pointvalue when c.pointvalue is not null then c.pointvalue else 1.00 end as points
from (select *,
convert_tz(created_date,'UTC','America/New_York') as local_created_date,
convert_tz(completed_date,'UTC','America/New_York') as local_completed_date,
HOUR(convert_tz(created_date,'UTC','America/New_York')) as created_hour,
case when created_by = 'bulk_dcr@usmaster.veevanetwork.com' then 'PDR' else 'DCR' end as main_type,
case when (left(t1.`resolution_notes`,1) = '[') and (t1.`created_by` <> 'bulk_dcr@usmaster.veevanetwork.com')
then left(t1.`resolution_notes`,9) else substr(t1.`description`,locate('PDR',t1.`description`,1),7) end as request_type ,
case when completed_date is null or created_by = 'bulk_dcr@usmaster.veevanetwork.com'  then NULL 
else  TIMESTAMPDIFF(MINUTE,convert_tz(created_date,'UTC','America/New_York'),convert_tz(completed_date,'UTC','America/New_York')) END as TimeToResolve
from `dcr-status`.dcr_report t1
where (resolution = 'CHANGE_PENDING' or convert_tz(completed_date,'UTC','America/New_York') between (now() - interval 16 day) and now())
) a
left join `dcr-status`.points b on a.request_type = b.type and a.source = b.source
left join `dcr-status`.points c on a.request_type = c.type and c.source is null;

delete a.* from dashboard_report a
inner join `dcr-status`.excluded b on a.change_request_id = b.change_request_id
where a.resolution = 'CHANGE_PENDING';
SHOW WARNINGS;
