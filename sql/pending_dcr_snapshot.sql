USE `dcr-status`;
insert into `dcr-status`.pending_dcr_snapshot
select null,count(a.change_request_id),convert_tz(NOW(),'UTC','America/New_York') from `dcr-status`.`dashboard_report` a
where main_type = 'DCR' and resolution = 'CHANGE_PENDING';
SHOW WARNINGS;
