USE `dcr-status`;
truncate table `dcr_report`;
LOAD DATA local infile '/mnt/data/opendata/temp/Hourly_DCR_Tableau_append.csv'
REPLACE INTO TABLE `dcr_report`
FIELDS TERMINATED BY ','ESCAPED BY '\\'  OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
IGNORE 1 LINES
(`change_request_id`,`change_request_country`,`change_request_type`,`change_request_original_type`,@linked_suspect_match_id,@owner,`subject`,@description,`vid_key`,@vid__v,`entity_type`,`source`,`originating_system`,`state_key`,`resolution`,@resolution_notes,`created_date`,`created_by`,@completed_date,@completed_by,working_hours)
SET
vid__v = nullif(@vid__v,''),
resolution_notes = replace(@resolution_notes,CHAR(10), ''),
description = replace(@description,CHAR(12,10), ''),
completed_by = nullif(@completed_by,''),
completed_date = nullif(@completed_date,''),
owner = nullif(@owner, ''),
linked_suspect_match_id = nullif(@linked_suspect_match_id, '')
;

delete from `dcr-status`.`dcr_report` where owner = 'vinodhkumar.ramachandran@usmaster.veevanetwork.com';

delete from `dcr-status`.`dcr_report` where state_key in ('CANCELLED','INQUEUE');
