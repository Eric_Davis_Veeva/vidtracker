#!/bin/sh

# AUTHOR:       Eric Davis
# EMAIL:        eric.davis@veeva.com
# DATE:         2016.12.12
# PURPOSE:

#       Pulls a specific report from the network FTP and loads it into the database

BASE="/mnt/data/opendata"
SCRIPT="${BASE}/usr/vidtracker"
WORK="${BASE}/work/"
LOG="${BASE}/logs/daily_nar.log"
SQL="${SCRIPT}/sql"
TMP="${BASE}/temp"
REMOTE="/outbound/report"
REMOTEDIR="Hourly_DCR_Tableau_*"
REPORTDIR="Hourly_DCR_Tableau_[0-9]+_[0-9]+"
FIXEDFILE="${TMP}/Hourly_DCR_Tableau_append.csv"
REPORTFILE="Hourly_DCR_Tableau_1.csv"
USER=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/usm_network|//p')
HOST=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/.*@//p'|sed -n -e 's/,.*//p')
dirfile="dirfile"
thefile="thefile"

lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; cd "${REMOTE}" ; ls -Art "${REMOTEDIR}" | tail -n 1; bye;" -u "${USER}" "${HOST}" > ${dirfile}
LATEST="$(cat $dirfile | egrep -o "${REPORTDIR}")"
echo "-------------------------------------------------------------------------------------------------------"
echo "STARTING AT $(date)"
echo "LATEST IS : "$LATEST
PULL="${REMOTE}/$LATEST"
echo ${PULL}
echo ${REPORTFILE}

lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; cd \"$PULL\" ; ls -Art ; bye;" -u $USER $HOST > ${thefile}
if grep --quiet ${REPORTFILE} ${thefile}; then
	echo 'Found it'
else
	echo 'Directory is empty.  Exit'
	exit
fi


lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; set xfer:clobber on; cd \"$PULL\" ; get \"$REPORTFILE\" ; bye;" -u $USER $HOST
# Clean up resulting file
echo 'Working with ' "$REPORTFILE"
echo 'Replacing Escaped quote'
perl -p -i -e 's/\\"/"/g' "$REPORTFILE"

echo 'Replacing last line'
perl -i -pe  'BEGIN{undef $/;} s/"\n^"[0-9]/"\~\~\~"9/smg' "$REPORTFILE"

echo 'Stripping inline newlines'
perl -i -pe  'BEGIN{undef $/;} s/\n/,/smg' "$REPORTFILE"

echo 'Readding newlines'
perl -i -pe  'BEGIN{undef $/;} s/\~\~\~/\n/smg' "$REPORTFILE"
echo 'Remove Control Char'
# perl -i -pe  'BEGIN{undef $/;} s/[\000-\007\013-\037\177-\377]/smg' "$REPORTFILE"
printf '\n\n ALL DONE \n\n'
echo 'Python append'
time /usr/local/bin/python2.7 "${SCRIPT}/getWorkingTime.py" -i ${REPORTFILE} -o ${FIXEDFILE} -d comma


echo  "Loading mysql"
time mysql --defaults-file="${SCRIPT}/.dash.cfg" --show-warnings -vv < "${SQL}/load_report.sql"

echo "making the good table"
time mysql --defaults-file="${SCRIPT}/.dash.cfg" --show-warnings -vv < "${SQL}/make_dash_report.sql"

time mysql --defaults-file="${SCRIPT}/.dash.cfg" --show-warnings -vv < "${SQL}/pending_dcr_snapshot.sql"

rm $dirfile
rm "${REPORTFILE}"
rm ${FIXEDFILE}
