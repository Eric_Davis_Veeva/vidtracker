#! /usr/bin/env python2.7

# Author        : Eric Davis
# Email         : eric.davis@veeva.com
# Date          : 2017-04-18

# Description:
#
#
# Usage :
#
#	./<script name> -i <input file> -o <outputfile> -f <space separated list of fields>
#
# Incoming file is defined as pipe delimited.   Outgoing is also pipe
# Field values are CASE SENSITITVE

import csv
import re
import argparse
import os
import logging
from datetime import datetime,date, timedelta,time
from dateutil import tz
from collections import deque
import time 
def get_hours(s, e,holiday_list):
        f_tz = tz.gettz('UTC')
        t_tz = tz.gettz('America/New_York')
        s = datetime.strptime(s,'%Y-%m-%dT%H:%M:%S+00:00' )
        e = datetime.strptime(e,'%Y-%m-%dT%H:%M:%S+00:00' )
        s = s.replace(tzinfo=f_tz)
        s = s.astimezone(t_tz)
        e = e.replace(tzinfo=f_tz)
        e = e.astimezone(t_tz)

#	print "EST ST {0} EST EN {1}".format(s,e)

        st = datetime.strptime("08:00","%H:%M").time()
        en = datetime.strptime("20:00","%H:%M").time()


        if s.time() < st:
                adj_st = datetime.combine(s.date(),st)
                st_time = adj_st.replace(tzinfo=t_tz)
        else:
                adj_st = datetime.combine(s.date(),s.time())
                st_time = adj_st.replace(tzinfo=t_tz)


        if e.time() > en:
                adj_en = datetime.combine(e.date(),en)
                en_time = adj_en.replace(tzinfo=t_tz)
        else:
                adj_en = datetime.combine(e.date(),e.time())
                en_time = adj_en.replace(tzinfo=t_tz)



        if (en_time - st_time).days < 1:        # same day

                work_sec = (en_time - st_time).total_seconds()
        else:
                #multiday
                # figure out same day work hours
                st_end_time =  datetime.combine(s.date(),en)
                st_end_time = st_end_time.replace(tzinfo=t_tz)

                en_start_time =  datetime.combine(e.date(),st)
                en_start_time = en_start_time.replace(tzinfo=t_tz)

#		print "DAY START S:{0} E:{1}".format(st_time,st_end_time)
#                print "DAY END S:{0} E:{1}".format(en_start_time,en_time)
                work_sec = 0  # initialize zero
                weekend_cnt = 0
		holiday_cnt = 0
                work_count = 0
                for single_date in dayrange(st_time, en_time):
			if single_date.date().strftime('%Y-%m-%d') in holiday_list:
				holiday_cnt = holiday_cnt + 1
                        elif single_date.weekday() < 5:
                                if single_date.date() <> e.date() and single_date.date() <> s.date():
                                        work_count = work_count + 1
			
                        elif single_date.weekday() >= 5:
                                weekend_cnt = weekend_cnt + 1
#                       print "Date: {} Weekday:{}".format(single_date,single_date.weekday())

#	                print "Weekend: {} Weekday:{} Holiday:{} ".format(weekend_cnt,work_count,holiday_cnt)

                	start_sec = (st_end_time - st_time).total_seconds()
	                end_sec = (en_time - en_start_time).total_seconds()

                	work_sec = start_sec + end_sec + (43200 * work_count)

#        		print "S_sec {} E_sec {} WEEKEND {} WORK {}".format(start_sec,end_sec,86400 * weekend_cnt,43200 * work_count)
	work_sec = work_sec / 3600
	if work_sec < 0:
		work_sec = 0
        return work_sec

def dayrange(s,e):
        for n in range(int ((e - s).days)):
                yield s + timedelta(n)

def main():

	holiday_list = ['2017-02-24','2017-05-29','2017-07-04']

        # Parser arguments
	fieldarray = []
        parser = argparse.ArgumentParser(description='<Update me for future use>')
        parser.add_argument('-i','--in', help='Input File',required=True,dest='inputFile')
        parser.add_argument('-o','--out', help='output File',required=True,dest='resultFile')
	parser.add_argument('-d','--delim', help='Delimiter used',required=False,dest='delim',default='subsep')
#        parser.add_argument('-f','--fields',nargs='+', help='field list. Space Separated CASE SENSITIVE',required=True,dest='fieldarray')
	parser.add_argument('--log', help='log level',required=False,dest='loglevel',default='info')
        args = parser.parse_args()

	numeric_level = getattr(logging, args.loglevel.upper(), None)
	if not isinstance(numeric_level, int):
		raise ValueError('Invalid log level: %s' % loglevel)
	logging.basicConfig(level=numeric_level)

	logging.debug(args.inputFile)

	accepted_delimiters = ['subsep','comma','pipe']

	csv.register_dialect('comma',delimiter = ',', quotechar = '"', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_ALL)
        csv.register_dialect('pipe',delimiter = '|', quotechar = '"', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_NONE)
        csv.register_dialect('subsep',delimiter = chr(28), quotechar = '', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_NONE)

	# Check if a delimiter was specified, otherwise use SUBSEP for legacy code
	if(args.delim not in accepted_delimiters):
		logging.error('You must use one of the accepted delimiters : '+ ', '.join(accepted_delimiters))
		exit(1)
	

	defaultdialect = str(args.delim)
	defaultdialect = defaultdialect.lower()

	logging.debug('Default dialect is : ' + defaultdialect)

        # Check if input file is proper and we have permissions to read it

        if os.access(args.inputFile, os.R_OK):
                print 'Can access : {0}'.format(args.inputFile)
                # file input is good
        else:
                print 'Bad inputfile :{0}'.format(args.inputFile)
                exit(1)

        print "Good File!"


#	fieldarray = args.fieldarray
	fieldarray = ['created_date','completed_date']	


	with open(args.inputFile, 'rb') as myFile:

		try:
                        outFile = open(args.resultFile,'wb')
                except IOError:
                        print 'Count not open : {0}'.format(args.resultFile)
                        exit(1)

		# Set up input / output
		writer = csv.writer(outFile, dialect=defaultdialect)
                reader = csv.reader(myFile, dialect=defaultdialect)


		# handle th header we need
                header = reader.next()
		header.append('working_hours')    # add additional header

		#get the index of each value passed from the field arguments
		foo = set(fieldarray)
		
		print foo
	
		header_pos =[i for i, item in enumerate(header) if item in foo]

		if len(header_pos) <> len(fieldarray):
			print "Not all columns were found in source file"
			print "Asking : {0}".format(fieldarray)
			print "Found : {0}".format(header_pos)
			print "Are you using a proper delimited file?"
			exit(1)

		print header_pos

                # write the header out first for the new file
                writer.writerow(header)
	
		print "Starting Row Loop ... "
		try:
	                for row in reader:
				try:
					line_array = []
					for i in header_pos:	# Loop through each column to change, and add the current value to an array
						line_array.append(row[i])				
					
				except Exception as e:
					print "Error parsing header: {0}".format(e)
					exit(1)

				if line_array[0] == '' or line_array[1] == '':
					working_hours = 0
				else:
					working_hours = get_hours(line_array[0],line_array[1],holiday_list)
#					print "This DCR was opened for : {} working hours".format(working_hours)
				# Start Output 
				row.append(working_hours)
				writer.writerow(row)		# Write new data row to output file

		except csv.Error as e:
			print "Error: {} line {}".format(e,reader.line_num)
	print "Complete !"	
        # end
        exit(0)


if __name__ == "__main__":
    main()

