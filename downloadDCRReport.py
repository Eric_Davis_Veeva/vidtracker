#! /usr/bin/env python3.5

# Author        : Eric Davis
# Email         : eric.davis@veeva.com
# Date          : 2017-04-25
# 
# This python process downloads the latest DCR reprot from NAR

import csv
import re
import argparse
import logging
import ftplib
import os, sys
import configparser
import time
#import smptlib

#from email.mime.text import MIMEText
from collections import deque
from datetime import datetime,date, timedelta,time
from dateutil import tz
from ftplib import FTP_TLS
from operator import itemgetter
import psycopg2


def sendmail(to,sub,usr,pwd,msg):

    msg = MIMEText(msg)
    msg['Subject'] = sub
    msg['From'] = 'donotreply@veevaopendata.com'
    msg['To'] = to
    try:
        s = smtplib.SMTP('smtp.mailgun.org', 587)
        s.login(usr, pwd)
        s.sendmail(msg['From'], msg['To'], msg.as_string())
    except Exception as e:
        log.error(e) 
    finally:
        s.quit()

def getHours(s,e,holiday_list):
    log = logging.getLogger(__name__)
    working_hours = 0
    #log.debug("RAW START: {}    END:{}".format(s,e))
    f_tz = tz.gettz('UTC')
    t_tz = tz.gettz('America/New_York')

    s = datetime.strptime(s,'%Y-%m-%dT%H:%M:%S+00:00' )
    e = datetime.strptime(e,'%Y-%m-%dT%H:%M:%S+00:00' )

    s = s.replace(tzinfo=f_tz)
    s = s.astimezone(t_tz)

    e = e.replace(tzinfo=f_tz)
    e = e.astimezone(t_tz)

    #log.debug("PARSE START: {}    END:{}".format(s,e))

    # Business hours are defined here for now
    st = datetime.strptime("08:00","%H:%M").time()
    en = datetime.strptime("20:00","%H:%M").time()

    # Adjust start and end times if they are beyond defined business hours

    if s.time() < st:
        adj_st = datetime.combine(s.date(),st)
        st_time = adj_st.replace(tzinfo=t_tz)
    else:
        adj_st = datetime.combine(s.date(),s.time())
        st_time = adj_st.replace(tzinfo=t_tz)

    if e.time() > en:
        adj_en = datetime.combine(e.date(),en)
        en_time = adj_en.replace(tzinfo=t_tz)
    else:
        adj_en = datetime.combine(e.date(),e.time())
        en_time = adj_en.replace(tzinfo=t_tz)

    if (en_time - st_time).days < 1:        # same day
        work_sec = (en_time - st_time).total_seconds()
    else:
        st_end_time =  datetime.combine(s.date(),en)
        st_end_time = st_end_time.replace(tzinfo=t_tz)
        en_start_time =  datetime.combine(e.date(),st)
        en_start_time = en_start_time.replace(tzinfo=t_tz)

        work_sec = 0  # initialize zero
        weekend_cnt = 0
        holiday_cnt = 0
        work_count = 0
        
        for single_date in dayrange(s.date(), e.date()):
            if single_date in holiday_list: # its a holiday
                holiday_cnt = holiday_cnt + 1
            elif single_date.weekday() < 5:         # its a workday
                if single_date != e.date() and single_date != s.date():
                    work_count = work_count + 1
            elif single_date.weekday() >= 5:        # its a weekend
                weekend_cnt = weekend_cnt + 1
   
            if st_time < st_end_time:
                start_sec = (st_end_time - st_time).total_seconds()
            else:
                start_sec = 0

            end_sec = (en_time - en_start_time).total_seconds()
            work_sec = start_sec + end_sec + (43200 * work_count) 
    
    work_sec = work_sec / 3600
    if work_sec < 0:
        work_sec = 0
    return work_sec

def dayrange(s,e):
    for n in range(int ((e - s).days)+ 1):
        yield s + timedelta(n)


def connDB(u,p,h,db):
    log = logging.getLogger(__name__)
    try:
        conn = psycopg2.connect("host={} dbname={} user={} password={}".format(h,db,u,p))
        log.info("Connected to DB")
    except Exception as e:
        log.error(e)
        exit(1)
    return conn


def writeTable(conn,ff,tbl):
    log = logging.getLogger(__name__)
    try:
        cur = conn.cursor()
        cur.execute("""truncate table dcr.dcr_report""")
        log.info("Table Truncated")
        cur.copy_expert('copy dcr.dcr_report from stdin WITH (FORMAT csv, FORCE_NULL(description,linked_suspect_match_id,vid__v,completed_date,completed_by,originating_system,owner), HEADER 1)',file=ff)
        log.info("Returned: {}".format(cur.rowcount))
    
    except Exception as e:
        log.error(e)
        exit(1)
    cur.close()
    conn.commit()

def runQ(conn,s):
    log = logging.getLogger(__name__)
    cur = conn.cursor()
    log.debug("Running SQL: {}".format(s))
    try:
        cur.execute(s)
        conn.commit()
        log.info("Returned: {}".format(cur.statusmessage))
    except Exception as e:
        log.error(e)
        exit(1)
    cur.close()

def connectFTP(u,p,h):

    log = logging.getLogger(__name__)
    log.info("Connected to FTP")
    log.debug("User: {} Pass: {} Host: {}".format(u,p,h))
    
    try:
        ftps = FTP_TLS(h)         
        ftps.login(u,p)
        ftps.prot_p()
    
        return ftps

    except ftplib.all_errors as e:
        log.error('Error connecting to FTP: {}'.format(e))
        exit(1)


def main():
    
    base = '/mnt/data/opendata_old'
    log = base+'/logs'
    script = os.path.abspath(os.path.dirname(sys.argv[0])) 
    env = base + "/env/env_python"
    logs = base + "/logs/dcr_report.log"
    work = base + "/work"

    # Holiday setup until its a file
    holiday_list = ['2017-01-02','2017-01-16','2017-02-20','2017-05-29','2017-07-04']

    

    # Parsing setup
    parser = argparse.ArgumentParser(description='<Update me for future use>')
    parser.add_argument('--log', help='log level',required=False,dest='loglevel',default='info')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    # Logging setup
    log = logging.getLogger(__name__)
    log.setLevel(level=numeric_level)   # File log is set via arguments
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh = logging.FileHandler(logs)
    fh.setFormatter(formatter)
    ch = logging.StreamHandler()
    #ch.setLevel(logging.INFO)      # ERROR to screen regardlesss
    
    log.addHandler(fh)
    log.addHandler(ch)

    log.debug("Log level : {}".format(log.getEffectiveLevel()))
    log.info("Logging Started")
    config = configparser.ConfigParser()
    config.read(env)
    cred = config['nar']
    db = config['dcr']
    em = config['email']
    newlist = []
    
    remotedir = '/outbound/report'
    remotefile = 'Hourly_DCR_Tableau_1.csv'
    localfile = work + "/dcrfile.csv"
    resultfile = work + "/dcrresult.csv"

    fieldarray = ['created_date','completed_date']

    csv.register_dialect('comma',delimiter = ',', quotechar = '"', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_ALL)
    csv.register_dialect('pipe',delimiter = '|', quotechar = '"', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_NONE)
    csv.register_dialect('subsep',delimiter = chr(28), quotechar = '', doublequote = True, skipinitialspace = True,lineterminator = '\n',quoting = csv.QUOTE_NONE)

    defaultdialect = "comma"

    nwk = connectFTP(cred['User'],cred['Pwd'],cred['Host'])
    nwk.cwd(remotedir)
    r = re.compile("Hourly_DCR_Tableau_[0-9]+_[0-9]+")
    dirlist = nwk.mlsd('',['modify','type'])
    for name,facts in nwk.mlsd('',['modify','type']):
        if facts['type'] == "dir"  and r.match(name):
            newlist.append([name,facts['modify']])         
    
    newlist.sort(key=itemgetter(1))
    latest = newlist[-1]
    log.debug("Latest value is {}".format(latest[0]))
    
    # We have the latest directory.  Get the file within it

    remotedir = remotedir + '/' + latest[0]
    log.debug("Using remote dir {}".format(remotedir))

    # open DB connection
    conn = connDB(db['user'],db['pwd'],db['host'],db['db'])
    
    try:
        f = open(localfile,'wb')    
        nwk.cwd(remotedir)
        nwk.voidcmd("TYPE I")
        nwk.retrbinary("RETR " + remotefile,f.write)
    except ftplib.all_errors as e:
        log.error('Error connecting to FTP: {}'.format(e))
        exit(1)
    finally:
        nwk.quit()

    f.close()
#    f = open(localfile,'rb')
    log.info("Adding workinghours to {}".format(localfile))
    with open(localfile, 'rt') as myFile:

        try:
            outFile = open(resultfile,'wt')
        except IOError:
            log.error('Count not open : {}'.format(resultfile))
            exit(1)

        writer = csv.writer(outFile, dialect=defaultdialect)
        reader = csv.reader(myFile, dialect=defaultdialect)

        header = reader.__next__()
        header.append('working_hours')

        #get the index of each value passed from the field arguments
        foo = list(fieldarray)
        header_pos =[i for i, item in enumerate(header) if item in foo]

        if len(header_pos) != len(fieldarray):
            log.critical("Not all columns were found in source file. ASking: {}  Found: {} ".format(fieldarray,header_pos))
            exit(1)

        writer.writerow(header)

        try:
            for row in reader:
                line_array = []
                for i in header_pos:
                    line_array.append(row[i])

                if line_array[1] == '':
                    working_hours = 0
                else:
                    working_hours = getHours(line_array[0],line_array[1],holiday_list)
   
                row.append(working_hours)
                writer.writerow(row)
            log.info("Completed Working Hours")
             
        except Exception as e:
            log.critical("Error parsing file: {}".format(e))
            exit(1)

    outFile.close()
    log.info("Downloaded to {}".format(localfile)) 
    outFile = open(resultfile,'rt')    

    writeTable(conn,outFile,'dcr_report')
    #sql = """delete from dcr.dcr_report where owner = 'vinodhkumar.ramachandran@usmaster.veevanetwork.com';"""
    #runQ(conn,sql)

    sql = """delete from dcr.dcr_report where state_key in ('CANCELLED','INQUEUE');"""
    runQ(conn,sql)
    
    sql = """insert into dcr.hourly_snapshot select count(distinct change_request_id), now() at time zone 'US/Eastern' from dcr.dcr_report where resolution = 'CHANGE_PENDING' and created_by <> 'bulk_dcr@usmaster.veevanetwork.com' and change_request_country = 'US' and change_request_id not in (870339576252598272,897371534165805056);"""
    runQ(conn,sql)

    conn.close()

    sub = """[Postgres] Prod update complete"""
    msg = """Postgres PROD update is completoe"""
    to = 'eric.davis@veeva.com'

#    sendmail(to,sub,email['user'],email['pwd'],msg)


if __name__ == "__main__":
    main()



